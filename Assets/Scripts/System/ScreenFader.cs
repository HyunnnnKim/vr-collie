﻿using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ScreenFader : MonoBehaviour
{
    #region Serialize Field
    [Space, Header("Fade")]
    [SerializeField] private float _fadeSpeed = 0.3f;
    #endregion

    #region Private Field
    private Volume _volume = null;
    private ShadowsMidtonesHighlights _smh = null;
    private float originShadowValue = 0f;
    #endregion

    private void Awake()
    {
        _volume = GetComponentInChildren<Volume>();
        _volume.profile.TryGet<ShadowsMidtonesHighlights>(out _smh);
        originShadowValue = _smh.shadows.value.magnitude;
    }

    #region Fader
    public Coroutine StartFadeIn()
    {
        StopAllCoroutines();
        return StartCoroutine(FadeIn());
    }

    private IEnumerator FadeIn()
    {
        while (_smh.shadows.value.magnitude >= 1)
        {
            _smh.shadows.value -= Vector4.one * _fadeSpeed * Time.deltaTime;
            _smh.midtones.value -= Vector4.one * _fadeSpeed * Time.deltaTime;
            _smh.highlights.value -= Vector4.one * _fadeSpeed * Time.deltaTime;
            yield return null;
        }
    }

    public Coroutine StartFadeOut()
    {
        StopAllCoroutines();
        return StartCoroutine(FadeOut());
    }

    private IEnumerator FadeOut()
    {
        while (_smh.shadows.value.magnitude <= originShadowValue)
        {
            _smh.shadows.value += Vector4.one * _fadeSpeed * Time.deltaTime;
            _smh.midtones.value += Vector4.one * _fadeSpeed * Time.deltaTime;
            _smh.highlights.value += Vector4.one * _fadeSpeed * Time.deltaTime;
            yield return null;
        }
    }
    #endregion
}