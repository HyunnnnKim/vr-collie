﻿using System.Collections.Generic;
using OVR;
using TMPro;
using UnityEngine;

namespace VRCollie
{
    public class KeyboardKeyController : MonoBehaviour
    {
        #region Serialize Field
        [Space, Header("Input")]
        [SerializeField] private int _characterLimit = 6;
        [Space, Header("Output")]
        [SerializeField] private TextMeshProUGUI _output = null;
        [Space, Header("Audio")]
        //[SerializeField] SoundFXRef _click = null;
        #endregion

        #region Private Field
        private KeyboardButtons[] _keyboardButtons = null;
        private Rigidbody[] childRigidbodies = null;

        private bool _isEnterPressed = false;
        #endregion

        private void Awake()
        {
            IgnoreCollisions();
            GetComponents();
        }

        #region Initialize
        private void IgnoreCollisions()
        {
            childRigidbodies = GetComponentsInChildren<Rigidbody>();
            Collider selectorCollider = GetComponent<Collider>();

            foreach (Rigidbody rigidbody in childRigidbodies)
            {
                Physics.IgnoreCollision(selectorCollider, rigidbody.GetComponent<Collider>(), true);
            }
        }

        private void GetComponents()
        {
            _keyboardButtons = GetComponentsInChildren<KeyboardButtons>();
        }
        #endregion

        private void OnEnable()
        {
            foreach (KeyboardButtons btn in _keyboardButtons)
            {
                btn.onKeyPush += Push;
            }
        }

        #region Push Event
        private void Push(KeyboardKeys key, Vector3 pos)
        {
            //_click.PlaySoundAt(pos);

            switch (key)
            {
                case KeyboardKeys.UnderBar:
                    KeyInput("_");
                    break;

                case KeyboardKeys.Menu:
                    Menu();
                    break;

                case KeyboardKeys.Delete:
                    Delete();
                    break;

                case KeyboardKeys.Enter:
                    Enter();
                    break;

                default:
                    KeyInput(key.ToString());
                    break;
            }
        }
        #endregion

        #region Key Action
        private void KeyInput(string key)
        {
            if (_isEnterPressed || _output.text.Length > _characterLimit) return;
            _output.text += key;
        }

        private void Menu()
        {
            // Menu Funciton

        }

        private void Delete()
        {
            if (_isEnterPressed) return;
            _output.text = _output.text.Substring(0, _output.text.Length - 1);
        }

        private void Enter()
        {
            if (_isEnterPressed) return;
            _isEnterPressed = true;

            // Enter Function
        }
        #endregion

        private void OnDisable()
        {
            foreach (KeyboardButtons btn in _keyboardButtons)
            {
                btn.onKeyPush -= Push;
            }
        }
    }
}