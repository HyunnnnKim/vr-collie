﻿using UnityEngine;

public class KeyboardButtons : PhysicsButton
{
    #region Public Field
    public delegate void KeyboardKeyFunction(KeyboardKeys key, Vector3 pos);
    public event KeyboardKeyFunction onKeyPush = null;
    #endregion

    #region Serialize Field
    [SerializeField] private KeyboardKeys _selectedKey = KeyboardKeys.None;
    #endregion

    #region Button Functions
    public override void OnButtonDown(Vector3 position)
    {
        onKeyPush?.Invoke(_selectedKey, position);
    }
    #endregion
}

public enum KeyboardKeys
{
    None, Q, W, E, R, T, Y, U, I, O, P,
    A, S, D, F, G, H, J, K, L,
    Z, X, C, V, B, N, M, UnderBar,
    Delete, Enter, Menu
}