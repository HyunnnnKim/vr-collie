﻿using UnityEngine;

public class PhysicsButton : MonoBehaviour
{
    #region Serialize Field
    [Space, Header("Physics Button Settings")]
    [SerializeField] protected float _pushPointOffset = -0.06f;
    [SerializeField] protected float _springPower = 3000f;
    #endregion

    #region Private Field
    private Vector3 _originPos = Vector3.zero;
    private Vector3 _pushPoint = Vector3.zero;
    private float _distance = 0f;
    private float _currentDistance = 0f;
    private GameObject _btn = null;
    #endregion

    #region Properties
    protected bool IsButtonPushing { get; private set; } = false;
    #endregion

    private void Awake()
    {
        InitVariables();
        ButtonSettings();
    }

    #region Initialize
    private void InitVariables()
    {
        _btn = transform.GetChild(0).gameObject;
        _originPos = _btn.transform.position;
        _pushPoint = _btn.transform.position + _btn.transform.up * _pushPointOffset;
        _distance = Vector3.Distance(_originPos, _pushPoint);
    }

    private void ButtonSettings()
    {
        SpringJoint joint = _btn.GetComponent<SpringJoint>();
        joint.spring = _springPower;
        joint.tolerance = 0f;
        joint.damper = 0.2f;
        joint.enablePreprocessing = true;
    }
    #endregion

    private void Update()
    {
        IsButtonPushed();
    }

    #region Invoke Event
    protected void IsButtonPushed()
    {
        _currentDistance = Vector3.Distance(_btn.transform.position, _pushPoint);
        if (_currentDistance < _distance * 0.5f && !IsButtonPushing)
        {
            IsButtonPushing = true;
            OnButtonDown(_btn.transform.position);
        }
        else if (_currentDistance > _distance * 0.6f && IsButtonPushing)
        {
            IsButtonPushing = false;
            OnButtonUp(_btn.transform.position);
        }
    }
    #endregion

    #region Button Functions
    public virtual void OnButtonDown(Vector3 buttonPosition) { }
    public virtual void OnButtonUp(Vector3 buttonPosition) { }
    #endregion

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(_pushPoint, 0.03f);
        Gizmos.DrawWireSphere(_originPos, 0.03f);
    }
}