﻿using UnityEngine;

public class PowerPunch : MonoBehaviour
{
    #region Serialize Field
    [Space, Header("Settings")]
    [SerializeField] private float punchForce = 100f;
    [SerializeField] private float triggerValue = 3f;
    #endregion

    #region Private Field
    private Rigidbody _rb = null;
    #endregion

    private void Awake()
    {
        GetComponents();
    }

    #region Initialize
    private void GetComponents()
    {
        _rb = GetComponent<Rigidbody>();
    }
    #endregion

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 1 << 9) return;
        ApplyForce(collision);
    }

    private void ApplyForce(Collision collision)
    {
        Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();

        if (rb && _rb.velocity.magnitude > triggerValue)
        {
            Debug.Log("------------------------------------------------" + rb.velocity.magnitude);
            rb.AddForce(_rb.velocity.normalized * punchForce, ForceMode.Impulse);
        }
    }
}